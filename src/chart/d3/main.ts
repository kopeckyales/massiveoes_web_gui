import * as d3 from 'd3';
import Point from './Point';
import MassiveD3Options from './Options/MassiveD3Options';
import {ScaleLinear} from 'd3-scale';
import {Axis} from 'd3-axis';
import {zoom} from 'd3-zoom';
import {BaseType, Selection} from 'd3-selection';
import DataSet from './DataSet';

export default abstract class MassiveD3 {
    protected svgFrame: Selection<SVGGElement, null, SVGElement, null>;
    protected svgRoot: Selection<SVGElement, null, null, null>;

    protected xAxis: Axis<number>;
    protected yAxis: Axis<number>;

    protected xAxisSVGElement: Selection<SVGGElement, number[], SVGElement, null>;
    protected yAxisSVGElement: Selection<SVGGElement, number[], SVGElement, null>;

    protected dataSets: DataSet[];


    protected currentZoomKFactor = 1;

    public constructor(protected svg: SVGElement, protected options: MassiveD3Options) {
        this.setOptions();
        window.addEventListener('resize', () => {
            this.redraw();
        });
        this.svgRoot = d3.select(svg);
        this.svgFrame = this.svgRoot.append<SVGGElement>('g')
            .attr('transform', 'translate(' + this.options.margin.left + ',' + this.options.margin.top + ')');
    }

    protected enablePlugins(xScale: ScaleLinear<number, number>, yScale: ScaleLinear<number, number>): void {
        this.enableZoom(xScale, yScale);
    }

    protected enableZoom(xScale: ScaleLinear<number, number>, yScale: ScaleLinear<number, number>): void {
        let zoomCallback = () => {
            this.svgFrame.selectAll('.charts')
                .attr('transform', d3.event.transform);
            this.currentZoomKFactor = d3.event.transform.k;
            d3.selectAll('.line').style('stroke-width', 1 / this.currentZoomKFactor);
            // d3.selectAll('.dot').attr('r', 1 / d3.event.transform.k);
            this.xAxisSVGElement.call(this.xAxis.scale(d3.event.transform.rescaleX(xScale)));
            this.yAxisSVGElement.call(this.yAxis.scale(d3.event.transform.rescaleY(yScale)));
        };
        this.svgRoot.call(d3.zoom()
            .scaleExtent([0.001, 1000])
            .extent([[100, 100], [this.options.FrameWidth - 100, this.options.FrameHeight - 100]])
            .on('zoom', zoomCallback)
        );
    }

    protected drawAxises(xScale: ScaleLinear<number, number>, yScale: ScaleLinear<number, number>): void {
        this.xAxis = <Axis<number>>d3.axisBottom(xScale)
            .ticks(5).tickSize(-this.options.FrameHeight);
        this.yAxis = <Axis<number>>d3.axisLeft(yScale)
            .ticks(5).tickSize(-this.options.FrameWidth);

        this.xAxisSVGElement = this.svgFrame.append<SVGGElement>('g')
            .attr('transform', 'translate(0,' + this.options.FrameHeight + ')')
            .call(this.xAxis);
        this.yAxisSVGElement = this.svgFrame.append<SVGGElement>('g')
            .call(this.yAxis);
    }

    protected drawDataLinesClipMask(): Selection<BaseType, Point[], SVGElement, Point[]> {
        this.svgFrame.append<SVGDefsElement>('defs')
            .append<SVGClipPathElement>('clipPath')
            .attr('id', 'cut-off')
            .append<SVGRectElement>('rect')
            .attr('width', Math.max(0, this.options.FrameWidth ))
            .attr('height', Math.max(0, this.options.FrameHeight));

        return this.svgFrame.append<SVGGElement>('g')
            .attr('clip-path', 'url(#cut-off)');
    }

    protected drawDataLinesContainer(): Selection<BaseType, Point[], SVGElement, Point[]> {
        return this.drawDataLinesClipMask()
            .append<SVGGElement>('g')
            .attr('class', 'charts');
    }

    protected setOptions() {
        this.options.width = this.svg.getBoundingClientRect().width;
        this.options.height = this.svg.getBoundingClientRect().height;
    }

    public redraw(dataSets: DataSet[] = null) {
        if (dataSets !== null) {
            this.dataSets = dataSets;
        }
        this.setOptions();
        [...this.svgFrame.node().children].map((e: HTMLElement) => e.remove());
        this.draw(this.dataSets);
    }

    protected abstract drawData(dataSets: DataSet[], xScale: ScaleLinear<number, number>, yScale: ScaleLinear<number, number>): void;

    public draw(dataSets: DataSet[]) {
        this.dataSets = dataSets;
        let points = (this.options.scaleByFirstDataSet)
            ? this.dataSets[0].data
            : [].concat.apply([], this.dataSets.filter((d: DataSet) => d.visible).map((d: DataSet) => d.data));

        let xScale = d3.scaleLinear()
            .domain(d3.extent(points, (p: Point) => p.x))
            .range([0, this.options.FrameWidth]);
        let yScale = d3.scaleLinear()
            .domain(d3.extent(points, (p: Point) => p.y))
            .range([this.options.FrameHeight, 0]);

        this.enablePlugins(xScale, yScale);
        this.drawAxises(xScale, yScale);
        this.drawData(dataSets, xScale, yScale);
    }
}