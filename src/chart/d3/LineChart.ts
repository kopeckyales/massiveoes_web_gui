import MassiveD3 from './main';
import DataSet from './DataSet';
import * as d3 from 'd3';
import Point from './Point';
import {ScaleLinear} from 'd3-scale';

export default class LineChart extends MassiveD3 {

    protected drawData(dataSets: DataSet[], xScale: ScaleLinear<number, number>, yScale: ScaleLinear<number, number>): void {
        let container = this.drawDataLinesContainer();

        let line = d3.line<Point>()
            .x((d: Point) => xScale(d.x))
            .y((d: Point) => yScale(d.y));

        for (let dataSet of dataSets.filter((d: DataSet) => d.visible)) {
            container.append<SVGPathElement>('path')
                .datum(dataSet.data)
                .attr('class', 'line')
                .attr('fill', 'none')
                .attr('stroke', dataSet.color)
                .attr('stroke-width', 1)
                .attr('d', line);
        }
    }
}