import {randomMaterialColor} from '../../factory/chartDataSetColor'

const state = {
  bpInspector: {
    line_simulation_data: {x: [], y: []},
    simulations_parts: [],
    selected_points_table: [],
    calculated_temperature: {
      type: '',
      value: NaN,
      deviation: NaN,
      line_data: {x: [], y: []}
    }
  }
}

const getters = {
  bpSimulationLineChartData: (state) => state.bpInspector.line_simulation_data,
  bpSimulationsParts: (state) => state.bpInspector.simulations_parts,
  bpSelectedPoints: (state) => state.bpInspector.selected_points_table,
  bpCalculatedTemperature: (state) => state.bpInspector.calculated_temperature
}

const actions = {
  // setActiveMeasurementSet: ({ commit }) => commit('SET_ACTIVE_MEASUREMENT_SET')
}

const mutations = {
  SOCKET_BP_TOOL__UPDATE: (state, data) => {
    Object.assign(state.bpInspector.line_simulation_data, data.line_simulation_data)
    state.bpInspector.selected_points_table = data.selected_points_table
    state.bpInspector.simulations_parts.map(e => e['data_key']).forEach(e => {
      if (data.simulations_parts.map(e => e['data_key']).indexOf(e) === -1) {
        state.bpInspector.simulations_parts.splice(state.bpInspector.simulations_parts.indexOf(e), 1)
      }
    })
    data.simulations_parts.forEach(e => {
      if (state.bpInspector.simulations_parts.map(e => e['data_key']).indexOf(e['data_key']) === -1) {
        e.data_set_color = randomMaterialColor()
        state.bpInspector.simulations_parts.push(e)
      }
    })
  },
  SOCKET_BP_TOOL__CALCULATED_TEMPERATURE: (state, data) => {
    Object.assign(state.bpInspector.calculated_temperature, data)
  },
  FLUSH_CALCULATED_TEMPERATURE: (state) => {
    // TODO: DRY, refactor
    state.bpInspector.calculated_temperature = {
      type: '',
      value: NaN,
      deviation: NaN,
      line_data: {x: [], y: []}
    }
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
