const state = {
  app: {
    loaderActive: false,
    connectionActive: false,
    running: true
  }
}

const getters = {
  appLoaderActive: (state) => state.app.loaderActive,
  appConnectionActive: (state) => state.app.connectionActive,
  appRunning: (state) => state.app.running
}

const actions = {
  activateAppLoader: ({ commit }) => commit('ACTIVATE_APP_LOADER'),
  deactivateAppLoader: ({ commit }) => commit('DEACTIVATE_APP_LOADER'),
  rerunApp: ({ commit }) => commit('RERUN_APP')
}

let appLoaderActivationTimeout

const mutations = {
  ACTIVATE_APP_LOADER: (state, message) => {
    // set loader only if operation takes some time, so there are no unnecessary blinks of the loader
    appLoaderActivationTimeout = setTimeout(() => {
      state.app.loaderActive = true
    }, 100)
  },
  DEACTIVATE_APP_LOADER: (state) => {
    clearTimeout(appLoaderActivationTimeout)
    state.app.loaderActive = false
  },
  RERUN_APP: (state) => {
    state.app.running = true
  },
  SOCKET_CONNECT: (state, status) => {
    state.app.connectionActive = true
  },
  SOCKET_DISCONNECT: (state, status) => {
    state.app.connectionActive = false
    state.app.running = false
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
