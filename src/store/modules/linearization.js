const state = {
  linearization: {
    measurement_data: {x: [], y: []},
    simulation_data: {x: [], y: []},
    parameters: [],
    linearize_pairs: []
  }
}

const getters = {
  linearizationChartData: (state) => {
    return {
      measurement_data: state.linearization.measurement_data,
      simulation_data: state.linearization.simulation_data
    }
  },
  linearizationParameters: (state) => state.linearization.parameters,
  linearizePairs: (state) => state.linearization.linearize_pairs
}

const actions = {
  addLinearizePair: ({ commit }) => commit('ADD_LINEARIZE_PAIR'),
  loadLinearizationData: ({ commit }) => commit('LOAD_LINEARIZATION_DATA'),
  removeLinearizePairs: ({ commit }) => commit('REMOVE_LINEARIZE_PAIRS')
}

const mutations = {
  ADD_LINEARIZE_PAIR: (state, data) => {
    state.linearization.linearize_pairs.push(data)
    state.linearization.linearize_pairs.sort((a, b) => a.measurement - b.measurement)
  },
  REMOVE_LINEARIZE_PAIRS: (state, data) => {
    data.forEach(e => {
      let index = state.linearization.linearize_pairs.indexOf(e)
      state.linearization.linearize_pairs.splice(index, 1)
    })
    state.linearization.linearize_pairs.sort((a, b) => a.measurement - b.measurement)
  },
  LOAD_LINEARIZATION_DATA: (state, data) => {
    state.linearization = Object.assign(state.linearization, data)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
