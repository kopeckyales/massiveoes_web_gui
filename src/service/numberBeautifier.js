let numeral = require('numeral')
numeral.zeroFormat('0')
export function formatNumber (value) {
  value = parseFloat(value)
  let format = '0.0000'
  if (Math.abs(value) < 0.0001) {
    format = '0.000e+00'
  }
  return numeral(value).format(format)
}
